# About

An (almost) empty project for front end web development based on webpack

## Usage

- From the project directory enter the following commands:

### for development
```
$ npm i
$ npm start
```

### create a deve build in the `dist` folder
> Contains sourcemaps en devtools
```
$ npm run build
```

### create a production build in the `dist` folder
> Cleaned up production-ready build
```
$ npm run prod
```

## Technologies used

- npm
- typescript
  - `tsconfig.json` or frontend development
  - custom.d.ts to allow image loading in `.ts` files
- webpack
  - loaders for typescript, scss/sass, css, fonts, etc.
  - css extracted to `dist/main.css`
  - plugin for HTML template
  - webpack-dev-server
